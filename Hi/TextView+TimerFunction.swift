//
//  TextView+TimerFunction.swift
//  Hi
//
//  Created by Vikram Haridas on 22/03/20.
//  Copyright © 2020 Vikram Haridas. All rights reserved.
//

import Foundation
import SwiftUI
extension UITextView {
    func typeOn(string: String) {
        let characterArray = string.characterArray
        var characterIndex = 0
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            self.text.append(characterArray[characterIndex])
            characterIndex += 1
            print("stuff")
            if characterIndex == characterArray.count {
                timer.invalidate()
            }
        }
    }
}
