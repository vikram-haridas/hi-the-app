//
//  String+CharacterWiseArray.swift
//  Hi
//
//  Created by Vikram Haridas on 22/03/20.
//  Copyright © 2020 Vikram Haridas. All rights reserved.
//

import Foundation

extension String {
    var characterArray: [Character]{
        var characterArray = [Character]()
        for character in self {
            characterArray.append(character)
        }
        print("Char array is : ")
        print(characterArray)
        return characterArray
    }
}
