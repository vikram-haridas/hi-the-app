//
//  ViewController.swift
//  Hi
//
//  Created by Vikram Haridas on 15/03/20.
//  Copyright © 2020 Vikram Haridas. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth
class ViewController: UIViewController {

    @IBOutlet weak var mobileNumberInput: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var otpInput: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("coming to viewcontroller")
        otpInput.isHidden = true
        // Do any additional setup after loading the view.
    }

    @IBAction func getPhoneNumber(_ sender: Any) {
    }
    var verifID:String? = nil
    @IBAction func getOTP(_ sender: Any) {
        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumberInput.text!, uiDelegate: nil) { (verificationID, error) in
          if let error = error {
            //self.showMessagePrompt(error.localizedDescription)
            return
          }
          // Sign in using the verificationID and the code sent to the user
          // ...
            self.verifID = verificationID
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.mobileNumberInput.isHidden = true
            self.otpInput.isHidden = false
            if verificationID != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verifID!, verificationCode: self.otpInput.text!)
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if (error != nil){
                        print(error.debugDescription)
                    }
                    else{
                        print(authData?.user.phoneNumber)
                    }
                })
            }
        }
        
    }
}

