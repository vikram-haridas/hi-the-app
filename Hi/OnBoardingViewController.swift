//
//  OnBoardingViewController.swift
//  Hi
//
//  Created by Vikram Haridas on 17/03/20.
//  Copyright © 2020 Vikram Haridas. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth
class OnBoardingViewController: UIViewController {
    
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var textViewThingy: UITextView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    var globalArray = [String]()
    var globalHighlightsArray = [String]()
    var messageCounter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view did load")
        // Do any additional setup after loading the view.
        let message0 = "... as long as they own a smartphone with Hi The App installed, have verified their phone number, created a profile, given access to their contacts, and enabled location services. \n\nYes, its that simple! "
        let message1 = "Before we begin, we must ask you something very important \n\nAre you Sandra? "
        let message2 = "Good, because Sandra is not allowed to use this app \n\nTo confirm that you are not Sandra, enter your name and phone number below ... "
        let message3 = "Sandra, we are so glad we could find you, all of these other users have such silly names. \n\nTo confirm that you are Sandra, enter your name and phone number below ... "
        let message4 = "An OTP (Overused Technology Passcode) has been sent to your phone number, enter it below ... "
        let message5 = "Lovely to meet you, \n\nQuick question, are you a horse? "
        let message6 = "Good, because horses are also not allowed to use this app. \n\nTo confirm that you are not infact a horse, please allow us to take a photo of you ... "
        let message7 = "WOW! A horse that uses a smartphone, we're so honoured to have you. \n\nTo confirm that you are infact a horse, please allow us to take a photo of you ... "
        let message8 = "And finally: to make sure your friends are facing the correct direction when they say Hi to you (which is the whole point of all of this), please allow us access to your contacts and location ... "
        let message9 = "UPLOADING YOUR DETAILS TO RUSSIAN HACKERZ... \n\nJust kidding. We wouldn't do that. Besides they know everything about you already "
        let message10 = "Login successful! Now just turn towards the light ... "
        globalArray = [message0, message1, message2, message3, message4, message5, message6, message7, message8, message9, message10]
        globalHighlightsArray = ["Hi", "Are you Sandra?", "enter your name and phone number below ... ", "enter your name and phone number below ... ", "enter it below ...", "Quick question, are you a horse?", "please allow us to take a photo of you ...", "please allow us to take a photo of you ...", "please allow us access to your contacts and location ...", "UPLOADING YOUR DETAILS TO RUSSIAN HACKERZ", "turn towards the light"]
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(typeThisYouLittleShit))
        
        self.view.addGestureRecognizer(gesture)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let message = "Bringing you a revolutionary app that helps you to say Hi to anyone, anywhere ... "
        let attributedString = NSMutableAttributedString.init(string: message)
        let range = (message as NSString).range(of: "Hi")
        let pinkFontColor = UIColor(red:0.86, green:0.04, blue:0.36, alpha:1.00);
        let range1 = NSRange(location: 0, length: message.count)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range1)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GillSans", size: 25.0), range: range1)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: pinkFontColor, range: range)
        textViewThingy.attributedText = attributedString
        textViewThingy.isUserInteractionEnabled = false
        textViewThingy.isEditable = false
        var counterLULZ = 0
        var typedThing = NSMutableAttributedString.init() //use this to split attributed string
        //let gesture = UITapGestureRecognizer(target: self, action: #selector(typeThisYouLittleShit))
        //self.view.addGestureRecognizer(gesture)
        for i in message {
            let range = NSRange(location:0,length:counterLULZ)
            typedThing = attributedString.attributedSubstring(from: range) as! NSMutableAttributedString
            //textViewThingy.text += "\(i)"
            textViewThingy.attributedText = typedThing
            RunLoop.current.run(until: Date()+0.06)
            counterLULZ += 1
        }
        //messageCounter += 1
    }
    
    @objc
    func typeThisYouLittleShit()
    {
        print([messageCounter])
        let message = globalArray[messageCounter]
        yesButton.isHidden = true
        noButton.isHidden = true
        nameField.isHidden = true
        phoneField.isHidden = true
        nextButton.isHidden = true
        let attributedString = NSMutableAttributedString.init(string: message)
        let range = (message as NSString).range(of: globalHighlightsArray[messageCounter])
        let pinkFontColor = UIColor(red:0.86, green:0.04, blue:0.36, alpha:1.00);
        let range1 = NSRange(location: 0, length: message.count)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range1)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GillSans", size: 25.0), range: range1)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: pinkFontColor, range: range)
        textViewThingy.attributedText = attributedString
        textViewThingy.isUserInteractionEnabled = false
        textViewThingy.isEditable = false
        var counterLULZ = 0
        var typedThing = NSMutableAttributedString.init() //use this to split attributed string
        //let gesture = UITapGestureRecognizer(target: self, action: #selector(typeThisYouLittleShit))
        //self.view.addGestureRecognizer(gesture)
        for i in message {
            let range = NSRange(location:0,length:counterLULZ)
            typedThing = attributedString.attributedSubstring(from: range) as! NSMutableAttributedString
            //textViewThingy.text += "\(i)"
            textViewThingy.attributedText = typedThing
            RunLoop.current.run(until: Date()+0.04)
            counterLULZ += 1
        }
        if (messageCounter == 1 || messageCounter == 5)
        {
            yesButton.isHidden = false
            noButton.isHidden = false
            
        }
        else if (messageCounter == 2 || messageCounter == 3 || messageCounter == 4)
        {
            nameField.isHidden = false
            if (messageCounter == 2 || messageCounter == 3)
            {
                if (true) //messageCounter == 2
                {
                    messageCounter -= 1
                }
                phoneField.isHidden = false
            }
            nextButton.isHidden = false
        }
        messageCounter += 1
    }
    @IBAction func yesButtonTap(_ sender: Any) {
        messageCounter += 1
        typeThisYouLittleShit()
    }
    @IBAction func noButtonTap(_ sender: Any) {
        typeThisYouLittleShit()
    }
    var verifID:String? = nil
    @IBAction func nextButtonTap(_ sender: Any) {
        print("inside tap func")
        print(messageCounter)
        if (messageCounter == 2 || messageCounter == 3)
        {
            //this is name and number screen
            print("Name and number screen")
            print("Value outside conditon for 2 : " + String(messageCounter))
            if (messageCounter == 2)
            {
                messageCounter += 1
                print("Value inside conditon for 2 : " + String(messageCounter))
            }
            if (messageCounter == 3){
                print("This happened")
                messageCounter += 1
            }
            Auth.auth().settings?.isAppVerificationDisabledForTesting = true
                   PhoneAuthProvider.provider().verifyPhoneNumber(phoneField.text!, uiDelegate: nil) { (verificationID, error) in
                     if let error = error {
                       print(error.localizedDescription)
                       return
                     }
                     // Sign in using the verificationID and the code sent to the user
                     // ...
                    self.verifID = verificationID
                    self.typeThisYouLittleShit()
                   }
            
        }
        else if (messageCounter == 5){
            print("Verify OTP here")
            UserDefaults.standard.set(verifID, forKey: "authVerificationID")
            self.phoneField.isHidden = true
            self.nameField.isHidden = false
            if verifID != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verifID!, verificationCode: self.nameField.text!)
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if (error != nil){
                        print(error.debugDescription)
                    }
                    else{
                        print(authData?.user.phoneNumber)
                        self.typeThisYouLittleShit()
                    }
                })
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
